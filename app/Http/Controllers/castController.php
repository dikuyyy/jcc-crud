<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create()
    {
    	return view('cast.create', [
    		'title' => 'Buat Cast Baru']);
    }

    public function store(Request $request)
    {
    	$request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
    ], 
   	[
        'nama.required' => 'Nama tidak boleh kosong',
        'umur.required'  => 'Umur tidak boleh kosong',
        'bio.required' => 'Bio tidak boleh kosong',
    ]
	);

	    DB::table('casts')->insert(
	    [
	     'nama' => $request['nama'],
	     'umur' => $request['umur'],
	     'bio' => $request['bio'],
	 	]
		);
	return redirect('/');
    }	

    public function index()
    {
    	$cast = DB::table('casts')->get();

    	return view('cast.cast', [
    		'title' => 'Daftar Cast',
    		'cast' => $cast,
    		'key' => 1,
    	]);
    }

    public function show($id)
    {
    	$cast = DB::table('casts')->where('id', $id)->first();

    	return view('cast.detail', [
    		'title' => 'Detail'. $cast->nama,
    		'cast' => $cast
    	]);
    }

    public function edit($id)
    {
    	$cast = DB::table('casts')->where('id', $id)->first();

    	return view('cast.edit', [
    		'title' => 'Edit'. $cast->nama,
    		'cast' => $cast
    	]);
    }

    public function update($id, Request $request )
    {
    	$request->validate([
    		'nama' => 'required',
    		'umur' => 'required',
    		'bio' => 'required',
    	],
    	[
    	'nama.required' => 'Nama tidak boleh kosong',
        'umur.required'  => 'Umur tidak boleh kosong',
        'bio.required' => 'Bio tidak boleh kosong',
   		 ]);

    	$update = DB::table('casts')->where('id', $id)->update(
    		[
    			'nama' => $request->nama,
    			'umur' => $request->umur,
    			'bio' => $request->bio,
    		]
    	);

    	return redirect('/');
    }

    public function destroy($id, Request $request)
    {
    	DB::table('casts')->where('id', $id)->delete();
    	return redirect('/');
    }
}
