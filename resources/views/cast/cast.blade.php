@extends('template.main')
@section('main')
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col" align="center">Aksi</th>

    </tr>
  </thead>
  <tbody>
  	@forelse ($cast as $item)

    <tr>
      
      <td scope="row">{{$key++}}</td>
      <td>{{$item->nama}}</td>
      <td>{{$item->umur}}</td>
      <td>{{$item->bio}}</td>
      <td>
      	
      	<form action="/cast/{{$item->id}}" method="POST">
      		<a class="btn btn-primary" href="/cast/{{$item->id}}" role="button">Detail</a>
      		<a class="btn btn-secondary" href="/cast/{{$item->id}}/edit" role="button">Ubah</a>
      		@csrf
      		@method('delete')
      		<input type="submit" class="btn btn-danger" value="Hapus">
      	</form>
      </td>
    </tr>
    @empty
    <p>Data Kosong, silakan buat data terlebih dahulu</p>
    @endforelse
    <a class="btn btn-primary mr-0" href="cast/create" role="button">Tambah Data</a>
   
  </tbody>
</table>
@endsection