@extends('template.main')

@section('main')
<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
<a class="btn btn-primary" href="/" role="button">Kembali</a>
@endsection