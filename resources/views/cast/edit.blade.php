@extends('template.main')

@section('main')
<form action="/cast/{{$cast->id}}" method="post">
	@csrf
  @method('put')
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control" id="nama" aria-describedby="emailHelp" name="nama" value="{{$cast->nama}}">
  </div>
	  @error('nama')
	    <div class="alert alert-danger">{{ $message }}</div>
	@enderror
  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="text" class="form-control" id="umur" name="umur" value="{{$cast->umur}}">
  </div>
	  @error('umur')
	    <div class="alert alert-danger">{{ $message }}</div>
	@enderror
  <div class="form-group">
    <label for="textarea">Biografi</label>
    <textarea class="form-control" id="textarea" rows="3" name="bio">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Input</button>
</form>
@endsection